<?php

namespace WakeOnWeb\Context;

use Behat\Behat\Context\Context;
use Behatch\HttpCall\Request;
use Behatch\HttpCall\Request\BrowserKit;
use Behatch\HttpCall\Request\Goutte;

/**
 * Class AuthenticationContext.
 */
class AuthenticationContext implements Context
{
    /**
     * @var BrowserKit | Goutte
     */
    private $request;

    /**
     * @var string
     */
    private $kongConsumer;

    /**
     * @var string
     */
    private $kongAnonymous;

    /**
     * @param Request $request
     * @param string  $kongConsumer
     * @param string  $kongAnonymous
     */
    public function __construct(Request $request, string $kongConsumer = '', string $kongAnonymous = '')
    {
        $this->request = $request;
        $this->kongConsumer = $kongConsumer;
        $this->kongAnonymous = $kongAnonymous;
    }

    /**
     * Authenticate user for next HTTP authentication.
     *
     * @param string $userId
     *
     * @Given /^The user "([^"]*)" is authenticated$/
     */
    public function theUserIsAuthenticated($userId)
    {
        $this->request->setHttpHeader('X-Authenticated-UserID', $userId);
        $this->request->setHttpHeader('X-Consumer-ID', $this->kongConsumer);
    }

    /**
     * Authenticate anonymously user for next HTTP authentication.
     *
     * @Given /^The user is authenticated anonymously$/
     */
    public function theUserIsAuthenticatedAnonymously()
    {
        $this->request->setHttpHeader('X-Anonymous-Consumer', $this->kongAnonymous);
    }
}
