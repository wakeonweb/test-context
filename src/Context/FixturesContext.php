<?php

namespace WakeOnWeb\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use Behat\Symfony2Extension\Context\KernelDictionary;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class FixturesContext.
 */
class FixturesContext implements Context
{
    use KernelDictionary;

    /**
     * @var string
     */
    private $fixturePath;

    public function __construct(string $fixturePath = '')
    {
        $this->fixturePath = $fixturePath;
    }

    /**
     * @Given I use fixture file :filename
     * @SuppressWarnings(PHPMD.ShortVariable)
     */
    public function iUserFixtureFile($filename)
    {
        $em = $this->getDoctrine();
        $loader = $this->getContainer()->get('fidry_alice_data_fixtures.loader.doctrine');
        $loader->load([$this->fixturePath.DIRECTORY_SEPARATOR.$filename]);

        $em->flush();
        $em->clear();
    }

    /**
     * @Given I use fixture files:
     */
    public function iUserFixtureFiles(TableNode $fileNames)
    {
        $entityManager = $this->getDoctrine();
        $loader = $this->getContainer()->get('fidry_alice_data_fixtures.loader.doctrine');
        $files = array_map(
            function ($item) {
                return $this->fixturePath.DIRECTORY_SEPARATOR.$item[0];
            },
            $fileNames->getRows()
        );
        $loader->load($files);

        $entityManager->flush();
        $entityManager->clear();
    }

    /**
     * @return EntityManagerInterface
     */
    private function getDoctrine()
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }
}
