<?php

namespace WakeOnWeb\Context;

use Behat\Gherkin\Node\PyStringNode;
use Behatch\Context\JsonContext as BaseJsonContext;

class JsonContext extends BaseJsonContext
{
    /**
     * Checks, that given JSON node is equal to given json value.
     *
     * @Then the JSON node :node should be equal to the following json:
     *
     * @throws \Exception
     */
    public function theJsonNodeShouldBeEqualToTheFollowingJson($node, PyStringNode $text)
    {
        $json = $this->getJson();

        $actual = $this->inspector->evaluate($json, $node);
        $actualAsArray = json_decode(json_encode($actual), true);
        $textAsArray = json_decode($text->getRaw(), true);

        if ($actualAsArray != $textAsArray) {
            throw new \Exception(
                sprintf("The node value is '%s'", json_encode($actual))
            );
        }
    }
}
