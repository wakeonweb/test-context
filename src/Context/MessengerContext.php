<?php

namespace WakeOnWeb\Context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Symfony2Extension\Context\KernelDictionary;
use Behatch\HttpCall\Request;
use Symfony\Component\Messenger\Transport\TransportInterface;
use Symfony\Component\Messenger\Envelope;

/**
 * Class MessengerContext
 * @package App\Tests\functional\Context
 */
class MessengerContext implements Context
{
    use KernelDictionary;

    /** @var Request|Request\BrowserKit */
    protected $request;

    private $profiles = [];

    /**
     * MessengerContext constructor.
     *
     * @param Request $request
     *
     * @throws \Exception
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param int    $expectedNbMessages
     * @param string $queueName
     *
     * @Then I should have :nbMessages messages created in :queueName
     * @throws \Exception
     */
    public function iShouldHaveMessagesInQueue(int $expectedNbMessages, string $queueName)
    {
        /** @var TransportInterface $transport */
        $transport = $this->getTransport($queueName);
        if (!isset($transport)) {
            throw new \Exception('No queue found with name '.$queueName);
        }

        if ($expectedNbMessages !== ($actualNbMessages = count($transport->get()))) {
            throw new \Exception("Expected $expectedNbMessages on queue $queueName, found $actualNbMessages");
        }
    }

    /**
     * @param string                           $queueName
     * @param PyStringNode $content
     *
     * @throws \Exception
     * @Then I should have a message in :queueName with content:
     */
    public function iShouldHaveAMessageInQueueWithContent(string $queueName, PyStringNode $content)
    {
        if (!$this->getContainer()->has('serializer')) {
            throw new \Exception('You must use symfony/serializer to be able to test messages content');
        }

        /** @var TransportInterface $transport */
        $transport = $this->getTransport($queueName);
        if (!isset($transport)) {
            throw new \Exception('No queue found with name '.$queueName);
        }

        $expectedContent = json_decode($content->getRaw(), true);
        $errors = [];
        $serializer = $this->getContainer()->get('serializer');

        /** @var Envelope $envelope */
        foreach ($transport->get() as $envelope) {
            try {
                $actualContent = $serializer->normalize($envelope->getMessage());
                $this->assertArrayMatch($expectedContent, $actualContent);

                return;
            } catch (\Exception $e) {
                $errors[] = $e->getMessage();
            }
        }

        $message = "No message found with given content.";

        foreach ($errors as $i => $error) {
            $message .= "\nMessage $i:\n".$error;
        }

        throw new \Exception($message);
    }

    /**
     * @param      $expectedArray
     * @param      $actualArray
     * @param null $path
     *
     * @throws \Exception
     */
    private function assertArrayMatch($expectedArray, $actualArray, $path = null)
    {
        foreach ($expectedArray as $key => $value) {
            if (!array_key_exists($key, $actualArray)) {
                $actualKeys = implode(', ', array_keys($actualArray));

                throw new \Exception("Expected key '$key'' was not found in message. Keys present are $actualKeys");
            }

            $actualValue = $actualArray[$key];
            $path        = null === $path ? $path.'['.$key.']' : $key;

            if (is_array($value)) {
                $this->assertArrayMatch($value, $actualValue, $path);
            } elseif ($value !== $actualValue) {
                throw new \Exception("Expected value '$value' for key $path but found '$actualValue''");
            }
        }
    }

    /**
     * @param string $queueName
     *
     * @return TransportInterface
     *
     */
    public function getTransport(string $queueName): TransportInterface
    {
        return $this->getContainer()->get('test.service_container')->get('messenger.transport.'.$queueName);
    }
}
