<?php

namespace WakeOnWeb\Context;

use Behat\Behat\Context\Context;
use Doctrine\ORM\Tools\SchemaTool;
use Behat\Symfony2Extension\Context\KernelDictionary;

/**
 * Class DoctrineContext.
 *
 * @author Olivier Maréchal <o.marechal@wakeonweb.com>
 */
class DoctrineContext implements Context
{
    use KernelDictionary;

    /**
     * @BeforeScenario
     */
    public function beforeScenario()
    {
        $this->buildSchema();
    }

    /**
     * @AfterScenario
     */
    public function afterScenario()
    {
        $this->getEntityManager()->clear();
    }

    protected function buildSchema()
    {
        $metadata = $this->getMetadata();
        if (!empty($metadata)) {
            $tool = new SchemaTool($this->getEntityManager());
            $tool->dropSchema($metadata);
            $tool->createSchema($metadata);
        }
    }

    /**
     * @return array
     */
    protected function getMetadata()
    {
        return $this->getEntityManager()->getMetadataFactory()->getAllMetadata();
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEntityManager()
    {
        return $this->getContainer()->get('doctrine.orm.entity_manager');
    }
}
